"use strict";

const path = require("path");
const paths = {
  ROOT_DIR: path.resolve( __dirname ) // .
  , SRC_DIR: path.resolve( __dirname, 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname, 'dist' ) // ./dist
  , MODULES_DIR: path.resolve( __dirname, 'node_modules' ) // ./node_modules
};

const HtmlWebpackPlugin = require('html-webpack-plugin');
const hwp = new HtmlWebpackPlugin({
  filename: 'index.html'
  , template: paths.SRC_DIR + '/index.html'
});

const CleanWebpackPlugin = require('clean-webpack-plugin');
const cwp = new CleanWebpackPlugin( ['dist'] );

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
  // filename: 'css/[name].css' // returns main.css
  filename: 'css/styles.css'
});

const webpack = require('webpack');
const hotModuleDev = new webpack.HotModuleReplacementPlugin({});
const wpp = new webpack.ProvidePlugin( {
  $: 'jquery'
  , jQuery: 'jquery'
} );
const nmp = new webpack.NamedModulesPlugin();

const config = {
  
  entry: {
    main: paths.SRC_DIR + '/app/app.js' 
    , vendorcode: paths.SRC_DIR + '/app/vendorcode.js'  
  }
  
  , devServer: {
    contentBase: paths.SRC_DIR
    , historyApiFallback: true
    , inline: true
    , hot: true
    , host: '127.0.0.1'
    , port: 8080
    , open: true
  } 

  , output: {
    filename: '[name].bundle.js'
    , path: paths.DIST_DIR 
    , publicPath: ''
  } 

  // see node_modules/webpack/lib/WebpackOptionsDefaulter.js , lines 168-185
  // see node_modules/webpack/lib/optimize/SplitChunksPlugin.js , lines 75,87,151, 198
  , optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /([\\/]node_modules[\\/]|[\\/]src[\\/]app[\\/]|[\\/]src[\\/]app[\\/]components)/  
          , chunks: 'all' // 'async' is default value but this is essential for splitting into commons~ bundles!
        }
      }
    }
  }
  
  , devtool: "source-map"

  , module: {
    rules: [
      {
        test: /\.js$/ 
        , exclude: /(node_modules|bower_components)/
        , use: [
          {
            loader: 'babel-loader'
            , options: {
              // presets: ['react','env']
              presets: [ '@babel/preset-react' , '@babel/env' ]
            }
          }
        ]
      }
      , {
        test: /\.scss$/ 
        , use: extractSass.extract( {
          fallback: 'style-loader'
          , use: [ { loader: "css-loader", options: { alias: { "../fonts/bootstrap": "bootstrap-sass/assets/fonts/bootstrap" } } } 
          , { loader: "sass-loader", options: { includePaths: [ paths.MODULES_DIR + "/bootstrap-sass/assets/stylesheets" ] } }  ]
        } )
      }
      , {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/
        , use: [
          {
            loader: "file-loader"
            , options: {
              name: '[name].[ext]'
              , publicPath: '../fonts/'
              , outputPath: 'fonts/' 
            }
          }
        ]
      }
      , {
        test: /\.(ico)$/
        , use: [
          {
            loader: 'file-loader'
            , options: {
              name: '[name].[ext]'
              , outputPath: './'
            }
          }
        ]
      }
    ]
  }
  , plugins: [
    cwp, nmp, hwp, extractSass, hotModuleDev, wpp
  ]
};

module.exports = config;