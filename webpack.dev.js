"use strict";

// const env = process.env.NODE_ENV;

const path = require("path");
const paths = {
  SRC_DIR: path.resolve( __dirname, 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname, 'dist' ) // ./dist
};

const merge = require( 'webpack-merge' );
const common = require( './webpack.common.js' );

const webpack = require('webpack');
const hotModuleDev = new webpack.HotModuleReplacementPlugin({});

const config = {
  
  mode: 'development' // (process.env.NODE_ENV === 'production' ) ? 'production' : 'development'

  , devServer: {
    contentBase: paths.SRC_DIR  // contentBase: paths.DIST_DIR
    , inline: true
    , historyApiFallback: true
    , hot: true
    , host: '127.0.0.1'
    , port: 8080
    , open: true
  } 

  , devtool: "inline-source-map"

  , plugins: [
    hotModuleDev
  ]
};

module.exports = merge( common, config );