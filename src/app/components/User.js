import React from 'react';
// import { browserHistory } from 'react-router-dom';

export class User extends React.Component{
  
  constructor(props){
    super(props);
    this.onNavigateHome = this.onNavigateHome.bind(this);
  };

  onNavigateHome(){
    this.props.history.push('/');
    // console.log( "this.props" , this.props );
  };

  render(){
    return(
      <div className="starter-template">
        <h1>Le User Page</h1>
        
        {/* <p>User ID: {this.props.match.params.id}</p> */}
        <p>User ID: {this.props.match.params.id}</p>

        <h2>Bootstrap starter template</h2>
        <p className="lead">Use this document as a way to quickly start any new project.<br /> All you get is this text and a mostly barebones HTML document.</p>
        <p className="lead">
          &nbsp; <span className="glyphicon glyphicon-pencil"></span>
          &nbsp; <span className="glyphicon glyphicon-envelope"></span>
          &nbsp; <span className="glyphicon glyphicon-search"></span>
          &nbsp; <span className="glyphicon glyphicon-heart"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud-upload"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud-download"></span>
        </p>
        <button className="btn btn-primary" onClick={ this.onNavigateHome } >Go Home!</button>
      </div>
    );
  };
} 
// https://stackoverflow.com/questions/44905067/react-router-v4-params