import React from 'react';
import {NavLink} from 'react-router-dom';

export const Header = () => {
  return(
    <nav className="navbar navbar-inverse navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <NavLink className="navbar-brand" to={'/'}>don lucho</NavLink>
        </div>
        <div id="navbar" className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li><NavLink to={'/'} activeClassName={"active"} >Home</NavLink></li>
            <li><NavLink to={'/user/55'} activeStyle={{color: "#FF0099", fontWeight: "bold" }} >User</NavLink></li>
            <li><NavLink to={'/about'} activeClassName={"active"} >About</NavLink></li>
          </ul>
        </div>
      </div>
    </nav>
  );
};