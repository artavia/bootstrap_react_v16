
import './favicon.ico';
import './css/boots.scss';
import './js/index.js';

import React from 'react';
import { ReactDOM, render } from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
// import { browserHistory } from 'react-router-dom';
// {/*<BrowserRouter history={browserHistory}>*/}

import { Root } from './components/Root';
import { Home } from './components/Home';
import { About } from './components/About';
import { User } from './components/User';

class CustomApp extends React.Component {
  render(){
    return(
      <BrowserRouter>
        <Root>
          <Switch>
            <Route exact={true} path={ '/' } component={Home} ></Route>
            <Route path={ '/about' } component={About} ></Route>
            <Route path={ '/user/:id' } component={User} ></Route>
            <Redirect to="/" />
          </Switch>
        </Root>
      </BrowserRouter>
    );
  };
}

render(<CustomApp/> , document.querySelector( '#leApp' ) );